#include <QtCore/QUrl>
#include <QtCore/QCommandLineOption>
#include <QtCore/QCommandLineParser>
#include <QGuiApplication>
#include <QStyleHints>
#include <QFile>
#include <QDir>
#include <QStandardPaths>
#include <QScreen>
#include <QQmlApplicationEngine>
#include <QtQml/QQmlContext>

#include <QDebug>

#ifdef Q_OS_ANDROID
#include <QtWebView/QtWebView>
#endif

// Workaround: As of Qt 5.4 QtQuick does not expose QUrl::fromUserInput.
class Utils : public QObject {
    Q_OBJECT
public:
    Utils(QObject* parent = 0) : QObject(parent) { }
    Q_INVOKABLE static QUrl fromUserInput(const QString& userInput);
    Q_INVOKABLE static QString errorPage();
};

QUrl Utils::fromUserInput(const QString& userInput)
{
    if (userInput.isEmpty())
        return QUrl::fromUserInput("about:blank");
    const QUrl result = QUrl::fromUserInput(userInput);
    return result.isValid() ? result : QUrl::fromUserInput("about:blank");
}

QString Utils::errorPage()
{
    QString resourcesPath = QStandardPaths::standardLocations(QStandardPaths::GenericCacheLocation).at(0);
    resourcesPath.append(QDir::separator());
    resourcesPath.append("ilfattonostro.costa");
    resourcesPath.append(QDir::separator());
    if (!QDir(resourcesPath).exists())
    {
        QDir().mkpath(resourcesPath);
    }

    QDir dir(resourcesPath);
    dir.setNameFilters(QStringList() << "*.*");
    dir.setFilter(QDir::Files);
    foreach(QString dirFile, dir.entryList())
    {
        dir.remove(dirFile);
    }

    QFile::copy(":/Error.html", resourcesPath + "Error.html");
    QFile::copy(":/header.jpg", resourcesPath + "header.jpg");
    QFile::copy(":/style.css", resourcesPath + "style.css");
    QFile::copy(":/css.css", resourcesPath + "css.css");
    QFile::copy(":/head.png", resourcesPath + "head.png");
    return resourcesPath + "Error.html";
}

#include "main.moc"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

#ifdef Q_OS_ANDROID
    QtWebView::initialize();
#endif

    QGuiApplication::setApplicationDisplayName(QCoreApplication::translate("main",
                                                                           "IlFattoNostro"));
    QCommandLineParser parser;
    QCoreApplication::setApplicationVersion(QT_VERSION_STR);
    parser.setApplicationDescription(QGuiApplication::applicationDisplayName());
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument("url", "The initial URL to open.");
    QStringList arguments = app.arguments();
    parser.process(arguments);
    const QString initialUrl = parser.positionalArguments().isEmpty() ?
        QStringLiteral("http://www.ilfattonostro.it/") : parser.positionalArguments().first();

    QQmlApplicationEngine engine;
    QQmlContext *context = engine.rootContext();
    context->setContextProperty(QStringLiteral("utils"), new Utils(&engine));
    context->setContextProperty(QStringLiteral("initialUrl"),
                                Utils::fromUserInput(initialUrl));
    QRect geometry = QGuiApplication::primaryScreen()->availableGeometry();
    if (!QGuiApplication::styleHints()->showIsFullScreen()) {
        const QSize size = geometry.size() * 4 / 5;
        const QSize offset = (geometry.size() - size) / 2;
        const QPoint pos = geometry.topLeft() + QPoint(offset.width(), offset.height());
        geometry = QRect(pos, size);
    }
    context->setContextProperty(QStringLiteral("initialX"), geometry.x());
    context->setContextProperty(QStringLiteral("initialY"), geometry.y());
    context->setContextProperty(QStringLiteral("initialWidth"), geometry.width() / 4);
    context->setContextProperty(QStringLiteral("initialHeight"), geometry.height());

#ifdef Q_OS_ANDROID
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
#else
    engine.load(QUrl(QStringLiteral("qrc:/main_webengine.qml")));
#endif
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
