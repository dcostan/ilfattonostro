import QtQuick 2.2
import QtQuick.Controls 2.0
import QtWebView 1.1
import QtQuick.Layouts 1.1

ApplicationWindow {
    property bool showProgress: webView.loading
                                && Qt.platform.os !== "ios"
                                && Qt.platform.os !== "winrt"
    visible: true
    x: initialX
    y: initialY
    width: initialWidth
    height: initialHeight
    title: webView.title

    footer: ToolBar {
        id: statusBar
        visible: showProgress
        RowLayout {
            anchors.fill: parent

            ProgressBar {
                id: control
                anchors.fill: parent
                visible: showProgress
                from: 0
                to: 100
                value: webView.loadProgress == 100 ? 0 : webView.loadProgress

                background: Rectangle {
                    color: "#e6e6e6"
                    radius: 3
                }
                contentItem: Item {
                    implicitWidth: 200
                    implicitHeight: 4

                    Rectangle {
                        width: control.visualPosition * parent.width
                        height: parent.height
                        radius: 2
                        color: "#17a81a"
                    }
                }
            }

        }
    }

    WebView {
        id: webView
        anchors.fill: parent
        url: initialUrl

        onLoadingChanged: {
            if (url.toString().indexOf("://www.ilfattonostro.it") == -1 && url.toString().indexOf("file://") == -1)
            {
                stop();
                if(!loadRequest.status)
                    Qt.openUrlExternally(url.toString());
            }
            if (loadRequest.status == 3)
                url = "file://" + utils.errorPage();
        }
    }

    onClosing: {
        if(webView.canGoBack && webView.url.toString().indexOf("file://") === -1)
        {
            close.accepted = false;
            webView.goBack();
        }
        else
        {
            close.accepted = true;
        }
    }
}
