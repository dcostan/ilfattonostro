TEMPLATE = app
TARGET = IlFattoNostro

android {
    QT += qml quick webview
}

!android {
    QT += qml quick
}

SOURCES += main.cpp

RESOURCES += qml.qrc

macos:QMAKE_INFO_PLIST = macos/Info.plist
ios:QMAKE_INFO_PLIST = ios/Info.plist

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

!android {
    target.path = $$PREFIX/
    manifest.files = manifest.json
    manifest.path = $$PREFIX/
    desktop.files = ilfattonostro.desktop
    desktop.path = $$PREFIX/
    apparmor.files = ilfattonostro.apparmor
    apparmor.path = $$PREFIX/
    icon.files = IFN1.png
    icon.path = $$PREFIX/

    INSTALLS += target manifest desktop apparmor icon
}
